require 'pry'

$timetable = {}
days = %w(Mon Tue Wed Thu Fri Sat Sun)
shifts = ['06:00 - 14:00', '14:00 - 22:00', '22:00 - 06:00']
locations = ['LOC1', 'LOC2', 'LOC3', 'LOC4', 'LOC5', 'LOC6', 'LOC7']
slots = days.count * shifts.count * locations.count

$employees_max_days = 5
names = "ABCDEFGHIJKLMNOPQRSTUVWXYZΓΔΘΛΞΠΣΦΨΩ"
$employees = (1..(slots / $employees_max_days.to_f).ceil).to_a.map{ |e| names[e] }

def can_work_on?(day, employee)
  # return false if day == 'Sun'

  workers_on_day = $timetable[day].values

  !(workers_on_day.include?(employee))
end

def days_employee_scheduled_for(employee)
  workers_instances = $timetable.values.map(&:values).flatten

  workers_instances.count{|w| w == employee}
end

def worked_enough?(employee)
  days_employee_scheduled_for(employee) >= $employees_max_days
end

def get_available(day)
  $employees.detect do |e|
    can_work_on?(day, e) && !worked_enough?(e)
  end
end


puts "For #{days.count} days with #{shifts.count} shifts in #{locations.count} locations, you need #{$employees.count} employees"

current_employee = $employees.first

days.each do |day|
  $timetable[day] = {}
end

locations.each do |location|
  shifts.each do |shift|
    days.each do |day|
      if current_employee.nil? || !(can_work_on?(day, current_employee) && !worked_enough?(current_employee))
        current_employee = get_available(day)
      end

      $timetable[day]["#{location} - #{shift}"] = current_employee
    end
  end
end

print "\t\t\t"
print $timetable.keys.join("\t")
print "\n"

$timetable.values.first.keys.each do |shift|
  print "#{shift} \t"

  days.each do |day|
    print "#{$timetable[day][shift] || '-'} \t"
  end

  print "\n"
end

if days_employee_scheduled_for(current_employee) < $employees_max_days
  puts "\n#{current_employee} scheduled for less than #{$employees_max_days} days."
end

puts "\nDone."
