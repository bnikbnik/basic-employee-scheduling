This is my attempt to write a very basic shift scheduling algorithm in ruby and optimizing it.

In this repo I have the basic three versions of the code as it evolved.

The problem:
Schedule from a pool of employees shifts for 1 week.
- Each employee can work at most 5 days, and of course it's preferable that
  this is used as much as possible
- The number of shifts and the number of locations is set. So for each day we
  have shifts * locations possible slots for an employee to work for.
- Basic support for employee availability (leave days etc!)
- Employees should work at the same shift and location as much as possible.
  (No random positioning/hours, happier employees)

# schedule_v1.rb

This is the very first crude version. I didn't want to do anything fancy not
find the optimal solution to this problem. I started just writing the code that
tells the computer what to do in the simplest way possible. At this point I
don't care about the code itself, I just want to get a feel of the problem.

This version doesn't still have any support for leave days nor a pool of
employees - it just decides how many it needs by dividing the slots with the
maximum days an employee can work for.

This is a crude version of simply adding employess to the slot serially. This
means that starting for the first slot it adds 5 instances of employee A, then 5
instances of employee B, changing slot at the end of the week.

# schedule_v2.rb

This version adds an employee pool, and a basic support for leave days. So
basically it now works as described!

Of course, the code is still crude, and of course it's awfully slow. With the
default case of 7 locations and a pool of around 41 employees it finishes quite
fast, but pumping up the numbers just at 10 locations and 100 employees a small
lag starts to feel apparent. At 2000 employees and 100 locations it doesn't
even finish in a reasonable time!

It's not a mystery that the code is slow. Writing it I just wrote the simplest
code possible to get the job done. I didn't account what is simple for the
computer but what is simple for a human to think about.

So the timetable that it's building is stored in a hash of hashes with string keys
for days and the location-shift combination. The string for the latter is also
created in the loop!

```ruby
timetable = {
  {
    "Mon": {
      "Loc1 - 10:00-14:00": "Z",
      "Loc2 - 14:00-18:00": "C",
      "Loc3 - 18:00-22:00": "B",
    },
    "Tue": { ... },
    ...
  }
}
```

This of course makes perfect sense to a human, but it's too much work for the
computer, especially since the loop keeps asking these questions that operate
on this hash:
1) Which employees work on Monday so far?
2) How many days in the week has X employee worked so far?

The last bottleneck is the constant sorting of the employee pool for each loop
iteration. To pick an available employee for a new slot we want employees that
are already scheduled(to avoid not filling the 5 days for employees, and that
they can work on this day on another shift/location. This naive implementation
sorts the employees by the number of days they work so far and selects from the
top! This doesn't only mean we sort the employee each iteration, but while
sorting we make expensive operations on the timetable hash described above just
to find out how many days each employee is scheduled to work for!

# schedule_v3.rb

At this point I know this simple algorithm, and have a pretty good idea for the
code.  It's time to rewrite, and optimize on that. The exact same code is
organized in a class now, and then I applied various optimizations based on the
things I noticed or knew about the previous version.

The first thing I did, is turn the timetable hash into a two dimensional array:

```ruby
timetable = [
  ["A", "A", "B", "B", "B", "B", "B"],
  ["C", "C", "C", "C", "C", "D", "D"],
]
```

Each row is a slot, each column a day. These are parallel to the days and slots
arrays. (the slot array is just a combination of locations+shifts)

The operations that answer the above questions are simpler now, and indeed
there was a small speed boost.

But it can still be optimized way better. I added a print '.' for each loop and
noticed that as the iterations go, the program became slower and slower. The
reason is that the timetable array gets larger and larger as we go so operating
on it will make the program slower and slower. To optimize for that I added two
more data structures.
1) An assigned_employees hash. The keys are the employees, and the values the
count of days they are assigned for. We can increase the count in the loop
easily, no need to count them each time!
2) An assigned_for_day hash. The keys are the days, and the value an array of
employees that work for that day. This can also easily be known during the
loop.

With these two hashes the timetable array isn't used during the loop except
from filling it with data. Running the script again indeed show the dots going
at a constant rate!

While debugging it I realized that it's a bit silly to keep duplicates in the
assigned_for_day hash. Why should we store each employee multiple times in
there if we don't need it? Ruby provides the Set class that is basically a
bucket of unordered unique data. Perfect! I wasn't sure this would give a
noticable speed-up, but it actually did when the employees where in the
thousands.

The last optimization is the constant sorting of employees. With a pool of
20000 employees this quickly became extremely slow, which makes perfect sense
of course. But why are we sorting it anyway? We just want to give priority to
the employees that have been scheduled the most days. Well, we already have
this now in the assigned_employees hash. So I took away the sort, and I just
find the employee with the max days in that hash (less than the maximum allowed
of course which is 5 per week). If such an employee doesn't exist, we just pick one
from the pool. No useless sorting required.

Now the result time isn't bound to the employee size pool anymore. 100
locations with 20K employees is almost instant. With 1000 locations it needs
about 20 seconds. Since for my application we will not be reaching these numbers
I think this is a good stopping point. I'm pretty sure it can be optimized
further though.

# schedule_future.rb

For a further improvement we can minimize the number of employees needed if we
prefer employees with no leave to minimize the amount of employees used. So if
A and B work for 3 + 2 days in the week because of their leaves, we can try
using another person with no leave in their place.

Also, if we prefer employess WITH leave to fill the last slot that needs less
days we'll minimize the issue that the last employee assigned doens't work for
the full 5 days.
