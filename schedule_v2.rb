require 'pry'

$timetable = {}
days = %w(Mon Tue Wed Thu Fri Sat Sun)
shifts = ['06:00 - 14:00', '14:00 - 22:00', '22:00 - 06:00']
locations = ['LOC1', 'LOC2', 'LOC3', 'LOC4', 'LOC5', 'LOC6', 'LOC7']
slots = days.count * shifts.count * locations.count

$employees_max_days = 5
names = "ABCDEFGHIJKLMNOPQRSTUVWXYZΓΔΘΛΞΠΣΦΨΩ@#%&*"
$employees = names.split("")

def can_work_on?(day, employee)
  # return false if day == 'Sun'

  # @Leaves
  return false if (%(A B C D).include?(employee) && %(Mon Tue Wed Thu Fri).include?(day) )
  return false if (%(E @ # *).include?(employee) && %(Wed Thu Fri Sat Sun).include?(day) )
  return false if (%(F G H I J W X Y Z).include?(employee) && %(Mon Fri).include?(day) )
  return false if (%(K L M N O P Q R S).include?(employee) && %(Sat Sun).include?(day) )


  workers_on_day = $timetable[day].values

  !(workers_on_day.include?(employee))
end

def days_employee_scheduled_for(employee)
  workers_instances = $timetable.values.map(&:values).flatten

  workers_instances.count{|w| w == employee}
end

def worked_enough?(employee)
  days_employee_scheduled_for(employee) >= $employees_max_days
end

def get_available(day)
  # Returns first the employees that have worked the most days
  # so that a) they get serial shifts, and so that they don't
  # end up not filling their days before a new employee is picked
  #
  $employees.sort_by do |e|
    -days_employee_scheduled_for(e)
  end.detect do |e|
    can_work_on?(day, e) && !worked_enough?(e)
  end
end


current_employee = $employees.first

days.each do |day|
  $timetable[day] = {}
end

locations.each do |location|
  shifts.each do |shift|
    days.each do |day|
      current_employee = get_available(day)

      $timetable[day]["#{location} - #{shift}"] = current_employee
    end
  end
end

print "\t\t\t"
print $timetable.keys.join("\t")
print "\n"

$timetable.values.first.keys.each do |shift|
  print "#{shift} \t"

  days.each do |day|
    print "#{$timetable[day][shift] || '-'} \t"
  end

  print "\n"
end

employee_count = $timetable.values.map(&:values).flatten.uniq.count
puts "For #{days.count} days with #{shifts.count} shifts in #{locations.count} locations, you need #{employee_count} employees."

$employees.each do |e|
  days = days_employee_scheduled_for(e)
  if days > 0 && days < $employees_max_days
    puts "#{e} scheduled for #{days} days."
  end
end

puts "\nDone."
