require 'pry'
require 'set'

class Timetable
  def initialize(days:, shifts:, employees: ,locations:, employees_max_days: 5)
    @days = days
    @shifts = shifts
    @locations = locations
    @employees_max_days = employees_max_days
    @employees = employees

    @timetable_slots = []
    @locations.each do |location|
      @shifts.each do |shift|
        @timetable_slots << "#{location} - #{shift}"
      end
    end

    @timetable = []
  end

  def run!
    @assigned_employees = Hash.new(0)
    @assigned_for_day = {}

    @timetable = []

    @timetable_slots.each_with_index do |slot, slot_idx|
      @days.each_with_index do |day, day_idx|
        current_employee = get_available(day)

        if current_employee
          @assigned_employees[current_employee] += 1
          @assigned_for_day[day] ||= Set.new
          @assigned_for_day[day].add(current_employee)
        end

        @timetable[slot_idx] ||= []
        @timetable[slot_idx][day_idx] = current_employee
      end
    end

    @timetable
  end

  def pretty_print
    print_timetable
    print_stats
  end

  def print_timetable
    print "\t\t\t"
    print @days.join("\t")
    print "\n"

    @timetable.each_with_index do |assignments, slot_idx|
      print "#{@timetable_slots[slot_idx]} \t"

      assignments.each do |e|
        print "#{e || '-'} \t"
      end

      print "\n"
    end
  end

  def print_stats
    employees_worked = @assigned_employees.keys
    print "For #{@days.count} days with #{@shifts.count} shifts in #{@locations.count} locations, "
    print "you need #{employees_worked.count} employees.\n"

    employees_worked.each do |e|
      days_worked = days_employee_scheduled_for(e)
      if days_worked < @employees_max_days
        puts "#{e} scheduled for #{days_worked} days."
      end
    end
  end

  private
  def get_available(day)
    not_found = true
    available, _ = @assigned_employees.max_by do |e, count|
      if count < @employees_max_days && can_work_on?(day, e)
        not_found = false
        count
      else
        -1
      end
    end

    if not_found
      @employees.detect do |e|
        can_work_on?(day, e) && !worked_enough?(e)
      end
    else
      available
    end
  end

  def days_employee_scheduled_for(employee)
    @assigned_employees[employee]
  end

  def can_work_on?(day, employee)
    # return false if day == 'Sun'

    # @Leaves
    return false if (%(A B C D).include?(employee) && %(Mon Tue Wed Thu Fri).include?(day) )
    return false if (%(E @ # *).include?(employee) && %(Wed Thu Fri Sat Sun).include?(day) )
    return false if (%(F G H I J W X Y Z).include?(employee) && %(Mon Fri).include?(day) )
    return false if (%(K L M N O P Q R S).include?(employee) && %(Sat Sun).include?(day) )

    workers_on_day = @assigned_for_day[day] || []

    !(workers_on_day.include?(employee))
  end

  def worked_enough?(employee)
    days_employee_scheduled_for(employee) >= @employees_max_days
  end
end


t = Timetable.new(
  days: %w(Mon Tue Wed Thu Fri Sat Sun),
  shifts: ['06:00 - 14:00', '14:00 - 22:00', '22:00 - 06:00'],
  locations: ['LOC1', 'LOC2', 'LOC3', 'LOC4', 'LOC5', 'LOC6', 'LOC7'],
  employees: %w(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z Γ Δ Θ Λ Ξ Π Σ Φ Ψ Ω @ # % & *),
  employees_max_days: 5
)
t.run!
t.pretty_print
